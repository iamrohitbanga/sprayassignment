float VERTEX_MARKER_RADIUS = 7;

class Triangulator
{
  ArrayList<Point> vertices;
  
  // debug data
  ArrayList<Circle> circumcircles;
  
  Triangulator()
  {
    vertices = new ArrayList<Point>();
    circumcircles = new ArrayList<Circle>();
  }


  void draw()
  {
    noFill();
    ellipseMode(RADIUS);
      
    fill(100, 200, 150);
    for(Point p : vertices)
    {
      ellipse(p.x, p.y, VERTEX_MARKER_RADIUS, VERTEX_MARKER_RADIUS);
    }
  }

  void addPoint(float x, float y)
  {
    vertices.add(new Point(x, y));
  }

  Mesh triangulate()
  {
    ArrayList<Integer> V = new ArrayList<Integer>();
    Point[] G = this.vertices.toArray(new Point[0]);

    ArrayList<SymbolicTriangle> allTriplets = new ArrayList<SymbolicTriangle>();
    for(int v1 = 0; v1 < G.length; v1++)
    {
      for(int v2 = v1+1; v2 < G.length; v2++)
      {
        for(int v3 = v2+1; v3 < G.length; v3++)
        {
          allTriplets.add(new SymbolicTriangle(G, v1, v2, v3));
        }
      }
    }

    // find triangles whose circumcircles do not contain other vertices
    SymbolicPointSet pointSet = new SymbolicPointSet(G);
    ArrayList<SymbolicTriangle> delaunayTriangles = new ArrayList<SymbolicTriangle>(); 
    Map<Integer, SortedSet<CornerTrianglePair>> cornerTrianglePairsByVertex = new TreeMap<Integer, SortedSet<CornerTrianglePair>>();
    for(int p = 0; p < G.length; p++)
      //cornerTrianglePairsByVertex.put(p, new TreeSet<CornerTrianglePair>(new ClockwiseAround(G, p)));
      cornerTrianglePairsByVertex.put(p, new TreeSet<CornerTrianglePair>(new ClockwiseAround(new SymbolicPoint(G, p))));
    int n = 0;
    int lastPercent = -1;
    for(SymbolicTriangle t : allTriplets)
    {
      int percentComplete = int(100 * float(n) / float(allTriplets.size()));
      if(percentComplete != lastPercent)
        println("Triangulation " + percentComplete + "% done");
      SymbolicPointSet otherPoints = pointSet.minus(t);
      if(t.circumcircle().intersect(otherPoints).size() == 0)
      {
        delaunayTriangles.add(t);

	//SymbolicPoint[] vertexIndices = t.toArray();
	SymbolicPoint[] symbolicVertices = t.toArray();
	
	for(int offset = 0; offset < 3; offset++)
	{
	  CornerTrianglePair cwt = new CornerTrianglePair();
	  cwt.triangle = t;
	  cornerTrianglePairsByVertex.get(symbolicVertices[offset].index).add(cwt);
	}
      }
      n++;
      lastPercent = percentComplete;
    }

    
    int cornerIndex = 0;
    for(SymbolicTriangle t : delaunayTriangles)
    {
      //Integer[] triangleVertexIndices = t.toArray();
      SymbolicPoint[] triangleSymbolicVertices = t.toArray();
      for(int triangleCornerIndex = 0; triangleCornerIndex < 3; triangleCornerIndex++)
      {
        int vertexIndex = triangleSymbolicVertices[triangleCornerIndex].index;
        V.add(vertexIndex);
        
	for(CornerTrianglePair ctp : cornerTrianglePairsByVertex.get(vertexIndex))
	{
          if(ctp.triangle == t)
	  {
	    ctp.cornerIndex = cornerIndex;
            break;
	  }
	}
	
	cornerIndex++;
      }
    }

    /*for(int cornerIndex = 0; cornerIndex < V.size(); cornerIndex++)
    {
      int vertexIndex = V.get(cornerIndex);
      for(CornerTrianglePair ctp : cornerTrianglePairsByVertex.get(vertexIndex))
      {
        ctp.cornerIndex = cornerIndex;
      }
    }*/

    return makeMesh(G, V, cornerTrianglePairsByVertex);
  }

  Mesh makeMesh(Point[] G, ArrayList<Integer> V, Map<Integer, SortedSet<CornerTrianglePair>> cornerTrianglePairsByVertex)
  {

    Mesh mesh = new Mesh();
    
    mesh.G = new PVector[G.length];
    mesh.V = new int[V.size()];
    mesh.S = new int[V.size()];
    mesh.C = new int[G.length];

    // per vertex stuff
    for(int v = 0; v < G.length; v++)
    {
      // vertex itself
      mesh.G[v] = G[v].toPVector();

      // swings
      ArrayList<Integer> orderedSwingIndices = new ArrayList<Integer>();
      for(CornerTrianglePair ctp : cornerTrianglePairsByVertex.get(v))
      {
        orderedSwingIndices.add(ctp.cornerIndex);
      }
      for(int localCornerIndex = 0; localCornerIndex < orderedSwingIndices.size(); localCornerIndex++)
      {
        int swingToIndex = orderedSwingIndices.get((localCornerIndex+1) % orderedSwingIndices.size());
        int swingFromIndex   = orderedSwingIndices.get(localCornerIndex);
        mesh.S[swingFromIndex] = swingToIndex;
      }

      // vertex to one corner
      mesh.C[v] = orderedSwingIndices.get(0);
    }
   


    for(int c = 0; c < V.size(); c++)
      mesh.V[c] = V.get(c);


    
    // now remove superswings (cut relies on -1 appearing in swing table a.k.a. no superswings)
    for(int cornerIndex = 0; cornerIndex < V.size(); cornerIndex++)
    {
      Corner corner = mesh.corner(cornerIndex);
      
      /*
      println("corner.id: "                                 + corner.id);
      println("corner.previous().id: "                      + corner.previous().id);
      println("corner.previous().unswing().id: "            + corner.previous().unswing().id);
      println("corner.previous().unswing().previous().id: " + corner.previous().unswing().previous().id);
      println("corner.swing().id: "                         + corner.swing().id);
      */


      //if(corner.previous().unswing().previous().id != corner.swing().id)
      if(corner.swing().next().vertex().id != corner.previous().vertex().id)
      {
        // superswing!
	mesh.S[cornerIndex] = -1;
        
      }

    }
    
    println(" ");
    for(int from = 0; from < mesh.S.length; from++)
      println("S[" + from + "] = " + mesh.S[from]);

    /*println(" ");
    for(int v = 0; v < mesh.G.length; v++)
      println("C[" + v + "] = " + mesh.C[v]);*/
   
    /*println(" ");
    for(int c = 0; c < mesh.S.length; c++)
      println("next(" + c + ") = " + mesh.corner(c).next().id);*/

    return mesh;
  }
}

class CornerTrianglePair
{
  Integer cornerIndex;
  SymbolicTriangle triangle;
}

class ClockwiseAround implements Comparator
{
  //Point[] G;
  SymbolicPoint origin;

  //ClockwiseAround(Point[] G, Integer originIndex) { this.G = G; this.originIndex = originIndex; }
  ClockwiseAround(SymbolicPoint origin) { this.origin = origin; }
  
  int compare(Object o1, Object o2)
  {
    SymbolicTriangle first  = ((CornerTrianglePair) o1).triangle;
    SymbolicTriangle second = ((CornerTrianglePair) o2).triangle;
    
    Vector firstDirection  = first.directionFrom(origin);
    Vector secondDirection = second.directionFrom(origin);

    float compareValue = firstDirection.angle() - secondDirection.angle();
    if(compareValue < 0.0)
      return -1;
    else if(compareValue == 0.0)
      return 0;
    else
      return 1;
  }
}

class SymbolicPointsClockwiseAround implements Comparator
{
  //Point[] G;
  Point origin;

  SymbolicPointsClockwiseAround(Point origin) { this.origin = origin; }
  
  int compare(Object o1, Object o2)
  {
    SymbolicPoint first  = (SymbolicPoint) o1;
    SymbolicPoint second = (SymbolicPoint) o2;
    
    Vector firstDirection  = new Vector(origin, first.point());
    Vector secondDirection = new Vector(origin, second.point());

    float compareValue = firstDirection.angle() - secondDirection.angle();
    if(compareValue <= 0.0)
      return -1;
    else if(compareValue == 0.0)
      return new Integer(first.index).compareTo(new Integer(second.index));
    else
      return 1;
  }
}

class SymbolicPointSet {
  Point[] points;
  SortedSet<Integer> indices;
  SymbolicPointSet(Point[] points)
  {
    this.points = points;
    this.indices = new TreeSet<Integer>();
    for(int p = 0; p < points.length; p++)
      indices.add(p);
  }
  SymbolicPointSet clone()
  {
    SymbolicPointSet copySet = new SymbolicPointSet(this.points);
    copySet.indices = new TreeSet<Integer>(this.indices);
    return copySet;
  }
  int size() { return this.indices.size(); }
  SymbolicPointSet minus(SymbolicTriangle t)
  {
    SymbolicPointSet result = new SymbolicPointSet(this.points);
    result.indices = new TreeSet<Integer>(this.indices);
    for(SymbolicPoint p : t.vertices)
      result.indices.remove(p.index);
    return result;
  }
}

class SymbolicPoint /*implements Comparable*/
{
  Point[] G;
  int index;
  SymbolicPoint(Point[] G, int index) { this.G = G; this.index = index; }
  Point point() { return G[this.index]; }
  /*int compareTo(Object o)
  {
    
  }*/
}

class SymbolicTriangle {
  Point[] points;
  SortedSet<SymbolicPoint> vertices;
  SymbolicTriangle(Point[] points, int v1, int v2, int v3)
  {

      
    this.points = points;
    Point triangleCenter = average(new Point[]{points[v1], points[v2], points[v3]});
    vertices = new TreeSet<SymbolicPoint>(new SymbolicPointsClockwiseAround(triangleCenter));
    vertices.add(new SymbolicPoint(points, v1));
    vertices.add(new SymbolicPoint(points, v2));
    vertices.add(new SymbolicPoint(points, v3));
    
    if(vertices.size() < 3)
    {
      println("wtf ");
      for(SymbolicPoint p : this.vertices)
      {
        println("have symPts: " + p.index);
      }
      println("should have: " + v1 + ", " + v2 + ", " + v3);
      assert(false);
    }
  }
  SymbolicPoint[] toArray() { return vertices.toArray(new SymbolicPoint[0]); }
  Circle circumcircle()
  {
    SymbolicPoint[] tri = this.toArray();
    if(tri.length < 3)
    {
      println("what the crap, tri has " + tri.length + " vertices");
      for(SymbolicPoint p : this.vertices)
      {
        println("symPt index: " + p.index);
      }
      assert(false);
    }
    //Point center = circumcenter(this.points[tri[0]], this.points[tri[1]], this.points[tri[2]]);
    Point center = circumcenter(tri[0].point(), tri[1].point(), tri[2].point());
    return new Circle(center, center.distanceTo(tri[0].point()));
  }
  //Vector directionFrom(Integer fromIndex)
  Vector directionFrom(SymbolicPoint fromPoint)
  {
    //int fromIndex = fromPoint.index;
    //Set<SymbolicPoint> otherPointIndices = new TreeSet<SymbolicPoint>(this.vertices);
    Set<SymbolicPoint> otherPoints = new TreeSet<SymbolicPoint>(this.vertices);
    //otherPointIndices.remove(fromIndex);
    otherPoints.remove(fromPoint);
    //Integer[] pointIndices = otherPointIndices.toArray(new Integer[0]);
    SymbolicPoint[] otherPointsArray = otherPoints.toArray(new SymbolicPoint[0]);
    //Vector a = new Vector(fromPoint, points[pointIndices[0]]);
    //Vector b = new Vector(fromPoint, points[pointIndices[1]]);
    Vector a = new Vector(fromPoint.point(), otherPointsArray[0].point());
    Vector b = new Vector(fromPoint.point(), otherPointsArray[1].point());
    Vector direction = a.plus(b).normalized();
    return direction;
  }
}

class Circle
{
  Point center;
  float radius;
  Circle(Point center, float radius)
  {
    this.center = center;
    this.radius = radius;
  }
  boolean contains(Point p) { return this.center.distanceTo(p) <= this.radius; }
  SymbolicPointSet intersect(SymbolicPointSet points)
  {
    SymbolicPointSet intersection = points.clone();
    SortedSet<Integer> indicesOfInternalPoints = new TreeSet<Integer>();
    for(Integer i : intersection.indices)
    {
      if(this.contains(intersection.points[i]))
      {
        indicesOfInternalPoints.add(i);
      }
    }
    intersection.indices = indicesOfInternalPoints;
    return intersection;
  }
}

/*float VERTEX_MARKER_RADIUS = 15;

class Triangulator
{
  ArrayList<Point> vertices;
  boolean initializedTriangulation;
  
  Triangulator()
  {
    vertices = new ArrayList<Point>();
    initializedTriangulation = false;
  }

  RightHalfspaceSegment previousHS = null;
  void draw()
  {
    fill(100, 200, 150);
    for(Point p : vertices)
    {
      ellipse(p.x, p.y, VERTEX_MARKER_RADIUS, VERTEX_MARKER_RADIUS);
    }
    if(initializedTriangulation)
    {
      if(frontier.size() > 0)
      {
        if(previousHS != null)
	{
	  drawPreviousCircumcenterAndBulge();
          stroke(227, 51, 11);
	  previousHS.draw();
	}
        stroke(160, 219, 9);
        RightHalfspaceSegment next = frontier.get(0);
	next.draw();
      }
    }
  }

  void addPoint(float x, float y)
  {
    vertices.add(new Point(x, y));
  }

  Point newestAddedPoint;
  void drawPreviousCircumcenterAndBulge()
  {
    if(vertices.size() < 3)
      return;

    noFill();

    Point active1 = previousHS.p1;
    Point active2 = previousHS.p2;
    Segment activeSegment = new Segment(active1, active2);
    Point candidate = newestAddedPoint;
    Point cc = circumcenter(active1, active2, candidate);
    float radius = cc.distanceTo(candidate);

    float bulge = activeSegment.bulgeOf(candidate);

    ellipseMode(RADIUS);
    ellipse(cc.x, cc.y, radius, radius);

    ellipseMode(CENTER);
  }
 
  ArrayList<Point> G;
  ArrayList<Integer> V;
  HashSet<Edge> E;
  ArrayList<RightHalfspaceSegment> frontier;
  HashSet<RightHalfspaceSegment> defrontieredSegments;

  void initTriangulation()
  {
    println("Starting triangulate()");
   
    assert(vertices.size() > 2);

    Point firstPoint = leftMostPoint(vertices);
    vertices.remove(firstPoint);
  
    Point secondPoint = leftMostPoint(vertices);
    vertices.remove(secondPoint);

    println("first: " + firstPoint.x + ", " + firstPoint.y);
    println("second: " + secondPoint.x + ", " + secondPoint.y);

    frontier = new ArrayList<RightHalfspaceSegment>();

    frontier.add(new RightHalfspaceSegment(firstPoint, secondPoint));
    frontier.add(new RightHalfspaceSegment(secondPoint, firstPoint));

    G = new ArrayList<Point>();
    G.add(firstPoint);
    G.add(secondPoint);
    V = new ArrayList<Integer>();
    E = new HashSet<Edge>();
    defrontieredSegments = new HashSet<RightHalfspaceSegment>();

    println("about to add first edge");
    E.add(new Edge(firstPoint, secondPoint));
  }

  Mesh step()
  {
    if(!initializedTriangulation)
    {
      initializedTriangulation = true;
      this.initTriangulation();
    }

    //for(int steps = 0; frontier.size() > 0; steps++)
    //{
      RightHalfspaceSegment frontierSegment = frontier.remove(0);
      previousHS = frontierSegment;

      // stopping condition: another visit to this frontier segment already added a triangle
      if(defrontieredSegments.contains(frontierSegment))
        return makeMesh();

      ArrayList<Point> candidatePoints = frontierSegment.halfspace().intersection(vertices);
      candidatePoints.remove(frontierSegment.p1);
      candidatePoints.remove(frontierSegment.p2);
      //println(candidatePoints.size() + " / " + vertices.size() + " in halfspace");

      // stop condition: no candidate points (meaning we are looking out from a segment on the convex hull)
      if(candidatePoints.size() == 0)
        return makeMesh();

      Point minBulgePoint = frontierSegment.findMinBulgePoint(candidatePoints);
      G.add(minBulgePoint); // FIXME will add points multiple times!!!!
      newestAddedPoint = minBulgePoint; 

      // i think this is visual clockwise order, as in mesh from makeTestMesh()
      V.add(G.indexOf(minBulgePoint));
      V.add(G.indexOf(frontierSegment.p2));
      V.add(G.indexOf(frontierSegment.p1));
      //println("about to add new edges");
      E.add(new Edge(minBulgePoint, frontierSegment.p1));
      E.add(new Edge(minBulgePoint, frontierSegment.p2));
      defrontieredSegments.add(frontierSegment);

      //println("added triangle");
      
      RightHalfspaceSegment f1 = new RightHalfspaceSegment(frontierSegment.p1, minBulgePoint);
      RightHalfspaceSegment f2 = new RightHalfspaceSegment(minBulgePoint, frontierSegment.p2);
      if(!defrontieredSegments.contains(f1))
        frontier.add(f1);
      if(!defrontieredSegments.contains(f2))
        frontier.add(f2);
    //}

    return makeMesh();
  }

  Mesh makeMesh()
  {
    Mesh mesh = new Mesh();
    mesh.G = new PVector[G.size()];
    for(int v = 0; v < G.size(); v++)
      mesh.G[v] = G.get(v).toPVector();
    mesh.V = new int[V.size()];
    for(int c = 0; c < V.size(); c++)
      mesh.V[c] = V.get(c);

    return mesh;
  }

  boolean isDone()
  {
    return frontier.size() == 0;
  }
}



class Edge
{
  Set<Point> points;

  Edge(Point a, Point b)
  {
    assert(a != b);
    points = new HashSet<Point>();
    points.add(a);
    points.add(b);
  }

  boolean equals(Object o)
  {
    Edge other = (Edge) o;
    Point[] myPoints    =  this.points.toArray(new Point[0]);
    Point[] otherPoints = other.points.toArray(new Point[0]);

    return (myPoints[0] == otherPoints[0] && myPoints[1] == otherPoints[1]) ||
           (myPoints[0] == otherPoints[1] && myPoints[1] == otherPoints[0]);
           
  }

  int hashCode()
  {
    Point[] pointsArray = this.points.toArray(new Point[0]);
    //println(pointsArray.length);
    Point a = pointsArray[0];
    Point b = pointsArray[1];
    return int(a.x) + int(a.y) + int(b.x) + int(b.y);
  }
}*/
