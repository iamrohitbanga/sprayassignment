float DEFAULT_RADIUS = 10.0;
Integer ballCounter = 0;

class SprayModeler {
  ArrayList<SprayBall> balls;
  
  SprayModeler() {
    balls = new ArrayList<SprayBall>();
//    PMatrix3D rMat = new PMatrix3D();
//    rMat.rotate(TWO_PI / 3, axis.x, axis.y, axis.z);

//    PVector center_1 = new PVector(DEFAULT_RADIUS / sin(TWO_PI / 6), 0, 0);
//    PVector center_2 = new PVector();
//    rMat.mult(PVector.add(center_1, new PVector()), center_2);
//    PVector center_3 = new PVector();
//    rMat.mult(PVector.add(center_2, new PVector()), center_3);
//
//    PVector center_4 = PVector.add(center_1, center_2);
//    center_4 = PVector.add(center_3, center_4);
//    center_4.mult(1.0/3);
//
//    PVector AO = PVector.sub(center_1, center_4);
//    PVector BO = PVector.sub(center_2, center_4);
//
//    PVector normal = AO.cross(BO);
//    normal.mult(1.0/normal.mag());
//    normal.mult(2 * DEFAULT_RADIUS * sin(TWO_PI/6));
//    center_4.add(normal);
//
//    println(center_1);
//    println(center_2);
//    println(center_3);

    // default balls
//    balls.addAll(Arrays.asList(new SprayBall(center_1, 200, 0, 0),
//			       new SprayBall(center_2, 0, 200, 0),
//			       new SprayBall(center_3, 0, 0, 200),
//			       new SprayBall(center_4, 100, 100, 100)
//			      ));

//    addEllipsoidBalls();

    addSphereBalls();

  }

  void addSphereBalls() {
    float R = 100;
    float zRange = 350;
    for (float z = -zRange/2; z <= zRange/2; z += 2*DEFAULT_RADIUS) {
      float phi = asin(z/R);
      float currentR = R * cos(phi);
      PVector p = new PVector(currentR, z, 0);
      float theta = 2 * atan(DEFAULT_RADIUS / currentR);
      int samples = (int)(TWO_PI / theta);
      PMatrix3D rMat = new PMatrix3D();
      rMat.rotate(theta, 0, 1, 0);
      for (int i = 0; i < samples; ++i) {
        println("here");
        balls.add(new SprayBall(p));
        rMat.mult(PVector.add(p, new PVector()), p);
      }
    }
  }

  void addEllipsoidBalls() {
    float a = 500.0;
    float b = 400.0;
    float c = 350.0;
    for (float u = -PI/2; u <= PI/2; u+=0.06) {
      for (float v = -PI; v <= PI; v+=0.06) {
        float x = a * cos(u) * cos(v);
        float y = b * cos(u) * sin(v);
        float z = c * sin(v);
        balls.add(new SprayBall(new PVector(x, y, z)));
      }
    }
  }

  void draw() {
    for (SprayBall b : balls) {
      b.draw();
    }
  }

  SprayBall getSmallestXBall() {
    SprayBall minXBall = null;
    for (SprayBall ball : balls) {
      if (minXBall == null || minXBall.center.x > ball.center.x)
	minXBall = ball;
    }
    return minXBall;
  }

  List<SprayBall> pointsInSphere(SprayBall current, float range) {
    List<SprayBall> result = new ArrayList<SprayBall>();
    for (SprayBall ball : balls) {
      if (ball.center.dist(current.center) <= range)
	result.add(ball);
    }
    return result;
  }

  void addBall(PVector center, PVector velocity) {
    balls.add(new SprayBall(center, velocity, 50, 50, 50));
  }

  void moveBalls() {

    List<Integer> removeBalls = new ArrayList<Integer>();

    int limit = 1000;
    for (int index = balls.size()-1; index >= 0; --index) {

      SprayBall b = balls.get(index);
      if (b.getCenter().mag() > limit) {
	removeBalls.add(index);
	continue;
      }

      PVector nextPosition = b.nextPosition();
      boolean acceptUpdate = true;
      for (SprayBall otherBall : balls) {
	if (b == otherBall)
	  continue;

	// what if we create a new ball too close to an exisiting ball
	if (otherBall.contains(b.getCenter())) {
	  acceptUpdate = false;
	  removeBalls.add(index);
	  break;
	}

	if (otherBall.contains(nextPosition)) {
	  acceptUpdate = false;
	  break;
	}
      }
      if (acceptUpdate)
	b.setCenter(nextPosition);
      else
	b.setVelocity(new PVector());
    }

    for (Integer removeBallIndex : removeBalls) {
//      if (removeBallIndex < balls.size())
//	balls.remove((int)removeBallIndex);
    }
  }

  int numBalls() {
    if (balls != null)
      return balls.size();
    else
      return 0;
  }
}

class SprayBall
{
  PVector center;
  PVector velocity;
  float radius = DEFAULT_RADIUS;
  int red = 250;
  int green = 211;
  int blue = 211;
  int id;

  SprayBall(PVector center, PVector velocity) {
    id = ballCounter++;
    this.center = new PVector(center.x, center.y, center.z);
    this.velocity = new PVector(velocity.x, velocity.y, velocity.z);
  }

  SprayBall(PVector center) {
    id = ballCounter++;
    this.center = new PVector(center.x, center.y, center.z);
    this.velocity = new PVector();
  }

  SprayBall(PVector center, PVector velocity, int red, int green, int blue) {
    id = ballCounter++;
    this.center = new PVector(center.x, center.y, center.z);
    this.velocity = new PVector(velocity.x, velocity.y, velocity.z);
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  SprayBall(PVector center, int red, int green, int blue) {
    id = ballCounter++;
    this.center = new PVector(center.x, center.y, center.z);
    this.velocity = new PVector();
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  void eraseBall() {
    pushMatrix();
    translate(this.center.x, this.center.y, this.center.z);
    strokeWeight(0);
    fill(0,0,0);
    sphere(this.radius);
    popMatrix();
  }

  void draw() {
    pushMatrix();
    strokeWeight(0);
    translate(this.center.x, this.center.y, this.center.z);
    fill(red, green, blue);
    sphere(this.radius);
    popMatrix();
  }

  void setVelocity(PVector v) {
    velocity = velocity.add(v, new PVector());
  }

  void setCenter(PVector c) {
    this.center = c;
  }

  PVector nextPosition() {
    PVector next = new PVector(center.x, center.y, center.z);
    next.add(velocity);
    return next;
  }

  PVector getCenter() {
    return new PVector(center.x, center.y, center.z);
  }

  boolean contains(PVector p) {
      return this.center.dist(p) < (this.radius + DEFAULT_RADIUS-2);
  }
}

