class Triangle
{
  PVector p1, p2, p3;

  public Triangle(PVector p1, PVector p2, PVector p3)
  {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
  }

  void draw() {
    fill(249, 212, 212);
    beginShape();
    vertex(p1.x, p1.y, p1.z);
    vertex(p2.x, p2.y, p2.z);
    vertex(p3.x, p3.y, p3.z);
    endShape(CLOSE);
  }

}

