float CORNER_LABEL_INDENT = 35.0;
float VERTEX_MARKER_RADIUS2 = 5;

class Mesh
{
  // The data structure Jarek has described in class:

  // G[v]: vertices indexed by vertex ID
  PVector[] G;

  // V[c]: maps corner ID to its vertex
  // NOTE: corner N*3, N*3 + 1, and N*3 + 2 represent a triangle.
  int[] V;

  // S[c]: maps corner ID to corner ID of its swing
  int[] S;

  // C[v]: maps vertex ID to one of its corners
  int[] C;

  // -----------------------------------------------

  // creates an empty mesh
  Mesh()
  {
    G = new PVector[0];
    V = new int[0];
    S = new int[0];
    C = new int[0];
  }

  // getting parts of the mesh

  Vertex myVertex(int id) { return new Vertex(this, id); }
  Corner corner(int id) { return new Corner(this, id); }

  // Methods

  void draw()
  {
    // draw edges.
    stroke(240);
    fill(100, 100, 255);
    for(int triangleIndex = 0; triangleIndex < V.length / 3; triangleIndex++)
    {
      // for each triangle, draw its edges.
      // draws every edge twice, but who cares.
      PVector p1 = G[V[triangleIndex * 3 + 0]];
      PVector p2 = G[V[triangleIndex * 3 + 1]];
      PVector p3 = G[V[triangleIndex * 3 + 2]];

      beginShape(POLYGON);
      vertex(p1.x, p1.y);
      vertex(p2.x, p2.y);
      vertex(p3.x, p3.y);
      endShape();
      line(p1.x, p1.y, p2.x, p2.y);
      line(p2.x, p2.y, p3.x, p3.y);
      line(p3.x, p3.y, p1.x, p1.y);
      PVector centroid = new PVector();
      centroid.set(p1);
      centroid.add(p2);
      centroid.add(p3);
      centroid.mult(0.33);

    }

    // now draw vertices
    stroke(0);
    fill(200, 100, 150);
    for(PVector p : G)
    {
      ellipse(p.x, p.y, VERTEX_MARKER_RADIUS2, VERTEX_MARKER_RADIUS2);
    }
  }

  // draw corner labels
  void debugDraw()
  {
    stroke(240);
    for(int cornerIndex = 0; cornerIndex < V.length; cornerIndex++)
    {
      Corner corner = this.corner(cornerIndex);

      Vertex cornerVertex = corner.vertex();
      Vertex awayVertex1 = corner.next().vertex();
      Vertex awayVertex2 = corner.previous().vertex();

      Vector direction1 = new Vector(cornerVertex.point(), awayVertex1.point());
      Vector direction2 = new Vector(cornerVertex.point(), awayVertex2.point());
      Vector cornerLabelDirection = direction1.plus(direction2).normalized();

      Point cornerLabelPosition = cornerVertex.point().plus(cornerLabelDirection.scaled(CORNER_LABEL_INDENT));

      fill(165, 242, 171);
      float fontSize = 20.0;
      textFont(createFont("Helvetica", fontSize));
      text("" + cornerIndex, cornerLabelPosition.x - fontSize/2.0, cornerLabelPosition.y + fontSize/2.0);
    }

    // now draw vertices
    stroke(0);
    int vertexIndex = 0;
    for(PVector p : G)
    {
      float fontSize = 36.0;
      textFont(createFont("Helvetica", fontSize));
      fill(245, 189, 59);
      text("" + vertexIndex, p.x - fontSize/2.0, p.y + fontSize/2.0);

      vertexIndex++;
    }

  }

  PVector getVertex(int vertex_id) {
    return G[vertex_id];
  }

  void setVertex(int vertex_id, PVector newPosition) {
    G[vertex_id].set(newPosition);
  }

  int getNumVertices() {
    return G.length;
  }

  int getNumTriangles() {
    return V.length / 3;
  }

  int[] getTriangleVertices(int id) {
    int[] vertices = new int[3];
    vertices[0] = V[3*id];
    vertices[1] = V[3*id + 1];
    vertices[2] = V[3*id + 2];
    return vertices;
  }

  PVector getVertexFromCorner(int id) {
    return G[V[id]];
  }

}

class Corner
{
  Mesh m;
  int id;

  Corner(Mesh m, int cornerID)
  {
    this.m = m;
    this.id = cornerID;
  }

  boolean equals(Object o)
  {
    Corner other = (Corner) o;
    return this.m == other.m && this.id == other.id;
  }

  Vertex myVertex()
  {
    return new Vertex(m, m.V[this.id]);
  }

  Vertex vertex()
  {
    return this.myVertex();
  }

  Corner next()
  {
    int triangleID = floor(this.id / 3);
    int cornerTriangleIndex = this.id % 3;
    int nextCornerTriangleIndex = (cornerTriangleIndex + 1) % 3;
    int nextCornerID = triangleID * 3 + nextCornerTriangleIndex;
    return new Corner(this.m, nextCornerID);
  }

  Corner previous()
  {
    return this.next().next();
  }

  Corner swing()
  {
    return new Corner(m, m.S[this.id]);
  }

  Corner unswing()
  {
    return this.next().swing().next();
  }
}

class Vertex
{
  Mesh m;
  int id;

  float x() { return m.G[this.id].x; }
  float y() { return m.G[this.id].y; }

  Point point() { return new Point(this.m.G[this.id]); }

  Vertex(Mesh m, int vertexID)
  {
    this.m = m;
    this.id = vertexID;
  }

  boolean equals(Object o)
  {
    Vertex other = (Vertex) o;
    return this.m == other.m && this.id == other.id;
  }
}




/************** testMesh creator *****************/
Mesh makeTestMesh()
{
  Mesh m = new Mesh();

  int numberOfVertices = 7;
  int numberOfCorners = 6 * 3; // 6 triangles, 3 corners each

  /** vertices **/
  m.G = new PVector[numberOfVertices];
  m.G[0] = new PVector(50.0,  14.0);
  m.G[1] = new PVector(100.0, 10.0);
  m.G[2] = new PVector(51.0,  50.0);
  m.G[3] = new PVector(150.0, 20.0);
  m.G[4] = new PVector(125.0, 60.0);
  m.G[5] = new PVector(75.0,  100.0);
  m.G[6] = new PVector(170.0,  100.0);
  // now scale it a bit
  for(PVector p : m.G)
  {
    p.x *= 3.0;

    p.y *= 3.0;
    p.y += 40.0;
  }

  /** corners to vertices **/
  m.V = new int[numberOfCorners];
  // T0
  m.V[0] = 0;
  m.V[1] = 1;
  m.V[2] = 2;
  // T1
  m.V[3] = 1;
  m.V[4] = 4;
  m.V[5] = 2;
  // T2
  m.V[6] = 4;
  m.V[7] = 5;
  m.V[8] = 2;
  // T3
  m.V[9] = 1;
  m.V[10] = 3;
  m.V[11] = 4;
  // T4
  m.V[12] = 3;
  m.V[13] = 6;
  m.V[14] = 4;
  // T5
  m.V[15] = 4;
  m.V[16] = 6;
  m.V[17] = 5;

  /** corners to swings (clockwise) **/
  m.S = new int[numberOfCorners];
  // T0
  m.S[0] = -1; // -1 means no swing. don't do superswing for now?
  m.S[1] = -1;
  m.S[2] = 5;
  // T1
  m.S[3] = 1;
  m.S[4] = 11;
  m.S[5] = 8;
  // T2
  m.S[6] = 4;
  m.S[7] = -1;
  m.S[8] = -1;
  // T3
  m.S[9] = 3;
  m.S[10] = -1;
  m.S[11] = -1;
  // T4
  m.S[12] = 10;
  m.S[13] = -1;
  m.S[14] = 15;
  // T5
  m.S[15] = 6;
  m.S[16] = 13;
  m.S[17] = -1;

  /** vertices to a corner **/
  m.C = new int[numberOfVertices];
  // i will pick the corner which lets us swing all the way around
  m.C[0] = 0;
  m.C[1] = 9;
  m.C[2] = 2;
  m.C[3] = 10;
  m.C[4] = 6;
  m.C[5] = 7;
  m.C[6] = 16;


  // mesh method tests
  assert(m.corner(0).next().equals(m.corner(1)));
  assert(m.corner(5).next().equals(m.corner(3)));

  assert(m.corner(0).previous().equals(m.corner(2)));
  assert(m.corner(12).previous().equals(m.corner(14)));

  assert(m.corner(9).swing().equals(m.corner(3)));
  assert(m.corner(14).swing().equals(m.corner(15)));
  assert(m.corner(15).swing().equals(m.corner(6)));

  assert(m.corner(14).unswing().equals(m.corner(11)));
  assert(m.corner(15).unswing().equals(m.corner(14)));

  assert(m.corner(8).myVertex().equals(m.myVertex(2)));
  assert(m.corner(17).myVertex().equals(m.myVertex(5)));

  println("Mesh operation tests succeeded.");

  return m;
}
