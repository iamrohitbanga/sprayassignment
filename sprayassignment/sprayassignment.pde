import oscP5.*;
import netP5.*;
import java.awt.event.*;

PVector eye;
PVector screenCenter;
PVector up = new PVector(0, 1, 0);

float xComp, zComp;
float angle;
float factor = 1.2;

float ROLL_BALL_RADIUS;

PVector xAxis = new PVector(1, 0, 0);
PVector yAxis = new PVector(0, 1, 0);
PVector zAxis = new PVector(0, 0, 1);
PVector origin = new PVector();
PVector axis = zAxis;

List<Triangle> triangles = new ArrayList<Triangle>();

Set<String> visited = new HashSet<String>();

int cameraDistance = 1000;
int dragMotionConstant = 5;
int movementSpeed = 50;
float sensitivity = 15;

boolean showBalls = true;

float standHeight = 300;

int moveX;
int moveZ;
boolean moveUP, moveRIGHT, moveDOWN, moveLEFT;

int totalBoxes = 30;
int stillBox = 200;

int showEyeDirection = 0;

SprayModeler modeler;
boolean illustrateCircumcenterAndBulge = false;

float DELTA_Y = 10;

int SCREENX = 1024;
int SCREENY = 650;

OscP5 oscP5;
boolean receiveOSC = true;
float[] matrix = new float[]{
  1.0, 0.0, 0.0,
  0.0, 1.0, 0.0,
  0.0, 0.0, 1.0
};

void setup()
{

  ROLL_BALL_RADIUS = DEFAULT_RADIUS * 4;

  background(0);
  size(SCREENX, SCREENY, P3D); 
  modeler = new SprayModeler();
  rectMode(CENTER);
  sphereDetail(10);
  addMouseWheelListener(new MouseWheelListener() { 
      public void mouseWheelMoved(MouseWheelEvent mwe) { 
	      mouseWheel(mwe.getWheelRotation());
	        }});

  eye = new PVector();
  eye.x = width/2;
  eye.y = height/2 - standHeight;
  eye.z = (height/2.0) / tan(PI * 60.0 / 360.0);
  screenCenter = new PVector();
  screenCenter.x = width/2;
  screenCenter.y = height/2;
  screenCenter.z = 0;
  xComp = screenCenter.x - eye.x;
  zComp = screenCenter.z - eye.z;
  angle = 0;

  moveX = 0;
  moveZ = 0;
  moveUP = false;
  moveRIGHT = false;
  moveDOWN = false;
  moveLEFT = false;

  //  oscP5 = new OscP5(this, 50000);
}

void mouseWheel(int delta) {
  float mag = eye.mag();
  PVector delta_vec = new PVector(eye.x, eye.y, eye.z);
  delta_vec.normalize();
  delta_vec.mult(0.05*mag*delta);
  eye.add(delta_vec);
}

void mousePressed() {
  // mouseEvent variable contains the current event information
  if (mouseEvent.getClickCount() == 2) {
    if (mouseEvent.getButton()==MouseEvent.BUTTON1) {
    //    initEye();
    }
  } else if (mouseEvent.getClickCount() == 1) {
    if (mouseEvent.getButton() == MouseEvent.BUTTON2) {
      // show the eye direction for the next few frames
      showEyeDirection = 10;
    }
  }
}

void initEye() {
//  eye = PVector.add(initialEye, new PVector());
}

void mouseDragged() {

  PVector delta = new PVector(mouseX-pmouseX, mouseY-pmouseY);
  delta.mult(-1);
  float mag = eye.mag();
  eye.add(delta);
}

void draw() 
{
  background(0);
  //lights();
  //spotLight(255,255,255,eye.x, eye.y-100,eye.z, screenCenter.x, screenCenter.y, screenCenter.z,PI,1);
  pointLight(255,255,255,eye.x,eye.y,eye.z);
  cameraUpdate();
  locationUpdate();

  //Draw Boxes
  for(int x1 = 0; x1 < totalBoxes; x1++){
    for(int z1 = 0; z1 < totalBoxes; z1++){
      pushMatrix();
      strokeWeight(0);
      translate(width/2+x1*100-(totalBoxes*100/2),height/2,z1*100-(totalBoxes*100/2));
      fill(255,255,0);
      box(90);  
      popMatrix();
    }
  }

  // Draw Triangles
  for (Triangle t : triangles) {
    t.draw();
  }

  strokeWeight(0);
  if (showBalls)
    modeler.draw();
  pushMatrix();
  strokeWeight(1);
  stroke(255, 0, 0);
  line(200, 0, 0, 0, 0, 0);
  stroke(0, 255, 0);
  line(0, 200, 0, 0, 0, 0);
  stroke(0, 0, 255);
  line(0, 0, 200, 0, 0, 0);
  popMatrix();

  if (true) {
    pushMatrix();
      translate(0, 0, 0);
      strokeWeight(1);
      stroke(255, 255, 255);
      PVector direction = PVector.add(eye, new PVector());
      line(direction.x, direction.y, direction.z, screenCenter.x, screenCenter.y, screenCenter.z);
    popMatrix();
    showEyeDirection--;
  }

  camera(
    eye.x, eye.y, eye.z,
    screenCenter.x, screenCenter.y, screenCenter.z,
    up.x, up.y, up.z
  );

  try {
    Thread.sleep(0);
  } catch (Exception ex) {
    ex.printStackTrace();
  }
 
//  receiveOSC = false;
}

void oscEvent(OscMessage message) {
  /* print the address pattern and the typetag of the received OscMessage */
  //print("### received an osc message.");
  //print(" addrpattern: "+message.addrPattern());
  //println(" typetag: "+message.typetag());
  String path = message.addrPattern();
  if(path.contains("rmatrix") && receiveOSC == true)
  {
    for(int c = 0; c < 9; c++)
      matrix[c] = message.get(c).floatValue();
    receiveOSC = false;
  }
}

public void keyPressed(){
  if(keyCode == UP && key == CODED){
    moveZ = -1;
    moveUP = true;
  }
  
  else if(keyCode == DOWN && key == CODED){
    moveZ = 1;
    moveDOWN = true;
  }
  
  else if(keyCode == LEFT && key == CODED){
    moveX = -1;
    moveLEFT = true;
  }
  
  else if(keyCode == RIGHT && key == CODED){
    moveX = 1;
    moveRIGHT = true;
  }
  else {
    switch (key) {
      case 'i': PVector velocity = PVector.sub(screenCenter, eye);
		velocity.normalize();
		velocity.mult(5);
		PVector center = PVector.add(eye, new PVector());
		modeler.addBall(center, velocity);
		println(modeler.numBalls());
		break;
      case 't': ballRolling();
		break;
      case 's': showBalls = !showBalls;
    }
  }
}

public void keyReleased(){
  if(keyCode == UP && key == CODED){
    moveUP = false;
    moveZ = 0;
  }
  else if(keyCode == DOWN && key == CODED){
    moveDOWN = false;
    moveZ = 0;
  }
    
  else if(keyCode == LEFT && key == CODED){
    moveLEFT = false;
    moveX = 0;
  }
  
  else if(keyCode == RIGHT && key == CODED){
    moveRIGHT = false;
    moveX = 0;
  }
}

void locationUpdate() {
    if(moveUP){
      eye.z += zComp/movementSpeed;
      screenCenter.z+= zComp/movementSpeed;
      eye.x += xComp/movementSpeed;
      screenCenter.x+= xComp/movementSpeed;
    }
    else if(moveDOWN){
      eye.z -= zComp/movementSpeed;
      screenCenter.z-= zComp/movementSpeed;
      eye.x -= xComp/movementSpeed;
      screenCenter.x-= xComp/movementSpeed;
    }
    if (moveRIGHT){
      eye.z += xComp/movementSpeed; 
      screenCenter.z+= xComp/movementSpeed;
      eye.x -= zComp/movementSpeed;
      screenCenter.x-= zComp/movementSpeed;
    }
    if (moveLEFT){
      eye.z -= xComp/movementSpeed; 
      screenCenter.z-= xComp/movementSpeed;
      eye.x += zComp/movementSpeed;
      screenCenter.x+= zComp/movementSpeed;
    }
    modeler.moveBalls();
}

void cameraUpdate() {
    int diffX = mouseX - width/2;
    int diffY = mouseY - height/2;
    
    if(abs(diffX) > stillBox) {
      xComp = screenCenter.x - eye.x;
      zComp = screenCenter.z - eye.z;
      angle = correctAngle(xComp,zComp);
       
      angle+= diffX/(sensitivity*10);
      
      if(angle < 0)
        angle += 360;
      else if (angle >= 360)
        angle -= 360;
      
      float newXComp = cameraDistance * sin(radians(angle));
      float newZComp = cameraDistance * cos(radians(angle));
      
      screenCenter.x = newXComp +  eye.x;
      screenCenter.z = -newZComp + eye.z;
    }

    if (abs(diffY) > stillBox)
      screenCenter.y += diffY/(sensitivity/1.5);
}

public float correctAngle(float xc, float zc){
  float newAngle = -degrees(atan(xc/zc));
  if (xComp > 0 && zComp > 0)
    newAngle = (90 + newAngle)+90;
  else if (xComp < 0 && zComp > 0)
    newAngle = newAngle + 180;
  else if (xComp < 0 && zComp < 0)
    newAngle = (90+ newAngle) + 270;
  return newAngle;
}

float angleBetweenVectors(PVector vectorA, PVector vectorB) {

    // Store some information about them for below
     float dot = vectorA.dot(vectorB);
     float lengthA = vectorA.mag();
     float lengthB = vectorB.mag();
      
     // Now to find the angle
     float theta = (float) Math.acos( dot / (lengthA * lengthB) );
     return theta;
}

void ballRolling() {
  println("ball Rolling started");
  triangles = new ArrayList<Triangle>();
  visited = new HashSet<String>();

  SprayBall current = modeler.getSmallestXBall();
  if (current == null) {
    println("current ball is null");
    return;
  }
  PVector rollCenter = PVector.add(current.center, new PVector(-ROLL_BALL_RADIUS, 0, 0));
  
  List<SprayBall> ballsInRange = modeler.pointsInSphere(current, 2*ROLL_BALL_RADIUS);
  println("Found balls in range: " + ballsInRange.size());
  SprayBall minBall = null;
  float minAngle = 1000000;
  for (SprayBall ball : ballsInRange) {
    float angle = angleBetweenVectors(PVector.sub(rollCenter, current.center),
       			              PVector.sub(ball.center, current.center));
    if (angle < minAngle) {
      minAngle = angle;
      minBall = ball;
    }
  }
  if (minBall == null) {
    println("minball1 not found");
    return;
  }
  else {
    println("current center is " + current.center);
    println("minball center is " + minBall.center);
  }

  PVector firstBallToSecondBall = PVector.sub(minBall.center, current.center);
  PVector firstBallToRollStart = PVector.sub(rollCenter, current.center);
  PVector midpoint = PVector.mult(firstBallToSecondBall, 0.5);
  float x = firstBallToSecondBall.mag() / 2.0;
  float y = sqrt( sq(ROLL_BALL_RADIUS) - sq(x) );
  PVector yBasis = new PVector();
  PVector.cross(firstBallToSecondBall, firstBallToRollStart, yBasis);
  yBasis.cross(firstBallToSecondBall);
  yBasis.normalize();
  rollCenter = PVector.add(PVector.mult(yBasis, y), midpoint);

  recursiveRoll(current, minBall, rollCenter);
  recursiveRoll(minBall, current, rollCenter);
}

float angleBetween(PVector a, PVector b)
{
  float dotProduct = PVector.dot(a, b);
  return acos( dotProduct / ( a.mag() * b.mag() ) );
}

void recursiveRoll(SprayBall from, SprayBall to, PVector rollBallStartingCenter) {
  PVector edgeMidpoint = new PVector(
    (from.center.x + to.center.x) / 2.0,
    (from.center.y + to.center.y) / 2.0,
    (from.center.z + to.center.z) / 2.0
  );
  PVector edgeMidpointToRollBallStartingCenter = PVector.sub(rollBallStartingCenter, edgeMidpoint);
  
  float minRollAngle = 100000.0;
  SprayBall minRollBall = null;
  PVector minRollCenter = null;
  for(SprayBall candidateBall : modeler.balls)
  {
    if(candidateBall != from && candidateBall != to)
    {
      List<PVector> rollBallRollCenters = intersectionOfThreeSameRadiusSpheres(from.center, to.center, candidateBall.center, ROLL_BALL_RADIUS); 
      for(PVector rollCenter : rollBallRollCenters)
      {
        //float edgeMidpointToRollBallRollCenter = PVector.sub(rollCenter, edgeMidpoint);
	PVector fromToTo = PVector.sub( to.center, from.center );
	PVector fromToCandidateBallCenter = PVector.sub( candidateBall.center, from.center );
	PVector rollUp = new PVector();
	PVector.cross( fromToCandidateBallCenter, fromToTo, rollUp );
        float rollAngle = angleBetween(edgeMidpointToRollBallStartingCenter, rollUp) + HALF_PI;
        if(minRollBall == null || rollAngle < minRollAngle)
        {
          minRollBall = candidateBall;
          minRollAngle = rollAngle;
	  minRollCenter = rollCenter;
        }
      }
    }
  }

  if(minRollBall != null)
  {
    println("gonna roll to " + minRollBall.center);
    List<Integer> idList = new ArrayList<Integer>(Arrays.asList(from.id, to.id, minRollBall.id));
    Collections.sort(idList);
    String key = idList.toString();
    
    if(!visited.contains(key))
    {
      triangles.add(new Triangle(from.center, to.center, minRollBall.center));
      visited.add(idList.toString());
      recursiveRoll(from, minRollBall, minRollCenter);
      recursiveRoll(minRollBall, to, minRollCenter);
    }
  }
  else
  {
    println("nothing close enough to roll to!");
  }

}

List<PVector> intersectionOfThreeSameRadiusSpheres(PVector s1, PVector s2, PVector s3, float radius)
{
  PVector s1s3 = PVector.sub(s3, s1);
  PVector s1s2 = PVector.sub(s2, s1);

  float d = s2.dist(s1);
  float i = PVector.dot(s1s3, s1s2) / s1s2.mag();
  float j = sqrt( s1.dist(s2) - sq(i) );

  float x  = ( sq(d) ) / (2*d);
  float y  = ( sq(i) + sq(j) ) / (2*j);
  float z = sqrt( sq(radius) - sq(x) - sq(y) );

  PVector xBasis = new PVector(s1s2.x, s1s2.y, s1s2.z);
  xBasis.normalize();
  PVector yBasis = PVector.sub( s3, PVector.mult(xBasis, i) );
  PVector zBasis = new PVector();
  PVector.cross(xBasis, yBasis, zBasis);
  zBasis.normalize();

  PVector result1 = PVector.add( s1, PVector.mult(xBasis, x) );
  result1 = PVector.add( result1, PVector.mult(yBasis, y) );
  result1 = PVector.add( result1, PVector.mult(zBasis, z) );
  
  PVector result2 = PVector.add( s1, PVector.mult(xBasis, x) );
  result2 = PVector.add( result2, PVector.mult(yBasis,  y) );
  result2 = PVector.add( result2, PVector.mult(zBasis, -z) );
  
  List<PVector> centers = new ArrayList<PVector>();
  centers.add(result1);
  centers.add(result2);
  
  return centers;
}
